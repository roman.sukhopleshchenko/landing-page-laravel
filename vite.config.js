import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/normalize.css',
                'resources/css/styles.css',
                'resources/css/adaptive.css',
                'resources/js/index.js',
            ],
            refresh: true,
        }),
    ],
});
