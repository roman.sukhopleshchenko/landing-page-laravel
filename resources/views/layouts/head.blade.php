<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1">
  <title>HOLY CHIC</title>
  @vite([
    'resources/css/normalize.css',
    'resources/css/styles.css',
    'resources/css/adaptive.css',
    'resources/js/index.js',
  ])
</head>