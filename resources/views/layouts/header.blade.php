  <header class="header">
    <div class="container">
      <div class="header__inner">
        <div class="logo">
          <a href="/" class="logo__link">
            <img class="logo__img" src="{{ asset('assets/images/logo.svg') }}" alt="logo">
          </a>
        </div>
        <nav class="menu">
          <button class="menu__btn" id="menuBtn">
            <img class="menu__img" src="{{ asset('assets/images/menu-btn.svg') }}" alt="menu"  id="menuBtn">
          </button>
        </nav>
      </div>
    </div>
  </header>