<article class="modal" id="modal">
    <div class="modal__menu" id="menu">
      <div class="modal__menu__flexcontainer">
        <div class="modal__menu__flexcontainer__text">
          <a href="#essence" id="essenceLink">О ПРОЕКТЕ</a>
          <div class="modal__menu__flexcontainer__text__line"></div>
          <a href="#conditions" id="conditionsLink">УСЛОВИЯ</a>
          <div class="modal__menu__flexcontainer__text__line"></div>
          <a id="contactsLink" href="#">КОНТАКТЫ</a>
        </div>
        <div class="modal__menu__flexcontainer__links">
          <div><a href="#"><img src="{{ asset('assets/images/facebook.svg') }}" alt="Facebook"></a></div>
          <div><a href="#"><img src="{{ asset('assets/images/instagram.svg') }}" alt="Instagram"></a></div>
          <div><a href="#"><img src="{{ asset('assets/images/phone.svg') }}" alt="Phone"></a></div>
        </div>
      </div>
    </div>
    <div class="modal__contacts" id="contacts">
      <div class="modal__contacts__flexcontainer">
        <div class="modal__contacts__flexcontainer__headtext">
          <h3 class="modal__contacts__flexcontainer__headtext__small"><span>НАШИ</span></h3>
          <h1 class="modal__contacts__flexcontainer__headtext__big">
            контакты
          </h1>
        </div>
        <div class="modal__contacts__flexcontainer__item">
          <div class="modal__contacts__flexcontainer__item__icon"><img src="{{ asset('assets/images/Geo.svg') }}" alt="addres"></div>
          <div>г. Кишинев,<br>ул. М.Г. Бэнулеску Бодони, 27 (вход с ул. Букурешть)</div>
        </div>
        <div class="modal__contacts__flexcontainer__item">
          <div class="modal__contacts__flexcontainer__item__icon"><img src="{{ asset('assets/images/Message.svg') }}" alt="e-mail"></div>
          <div>holychic.manager@gmail.com</div>
        </div>
        <div class="modal__contacts__flexcontainer__item">
          <div class="modal__contacts__flexcontainer__item__icon"><img src="{{ asset('assets/images/instagram-white.svg') }}" alt="instagram"></div>
          <div>@holy_chic.md</div>
        </div>
        <div class="modal__contacts__flexcontainer__item">
          <div class="modal__contacts__flexcontainer__item__icon"><img src="{{ asset('assets/images/facebook-white.svg') }}" alt="facebook"></div>
          <div>@holychic.md</div>
        </div>
        <div class="modal__contacts__flexcontainer__item">
          <div class="modal__contacts__flexcontainer__item__icon"><img src="{{ asset('assets/images/phone-white.svg') }}" alt="phone"></div>
          <div>+373 603 43 478</div>
        </div>
      </div>
    </div>
  </article>