<!DOCTYPE html>
<html lang="ru">
@include('layouts.head')
<body>
  @include('layouts.header')
  @include('layouts.modal') 
  <main>
    @yield('content')
  </main>
</body>
</html>