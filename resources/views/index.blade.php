@extends('layouts.base')
@section('content')

    <article class="intro">
      <div class="intro__container">
        <div class="intro__container__imgContainer">
          <img class="intro__img" src="{{ asset('assets/images/foto1.jpg') }}" alt="foto-intro">
        </div>
          <div class="intro__text">
          <div class="intro__text__big">
            Когда некуда вешать и нечего носить...
          </div>
          <div class="intro__text__small">
            сервис для покупки и продажи красивых вещей
          </div>
        </div>
        <div class="intro__arrow">
          <img class="intro__arrow__img" src="{{ asset('assets/images/Arrow.svg') }}" alt="Arrow">
        </div>
      </div>
    </article>
    <article class="essence" id="essence">
      <div class="essence__headtext">
        <h3 class="essence__headtext__small"><span>O ПРОЕКТЕ</span></h3>
        <h1 class="essence__headtext__big">
          суть
        </h1>
      </div>
      <div class="imagesAbstract">
        <div class="imagesAbstract__col1">
          <img src="{{ asset('assets/images/imgAbstract1.jpg') }}" alt="Shopping bag">
        </div>
        <div class="imagesAbstract__col2">
            <img src="{{ asset('assets/images/imgAbstract2.jpg') }}" alt="Jacket">
        </div>
        <div class="imagesAbstract__col3">
          <div class="redcube"></div>
        </div>
      </div>
      <div class="essence__text">
        <p><b>Holy Chic Studio</b> – первый и единственный ресэйл проект в Молдове, который позволяет продать или купить предметы личного гардероба премиум класса, новые либо в безупречном состоянии.</p>
        <p>Мы являемся раскрученной площадкой с уютным шоурумом в центре города и большим количеством лояльных покупателей. Мы берем на себя все «тяготы» сделки между вами и новым владельцем ваших вещей.</p>
      </div>
    </article>
    <article class="howitworks">
      <div class="howitworks__headtext">
        <h3 class="howitworks__headtext__small"><span>КАК ЭТО</span></h3>
        <h1 class="howitworks__headtext__big">
          работает
        </h1>
      </div>
      <div class="howitworks__text">
        <p><b>Договор комиссии</b> - это договор, согласно которому одна его сторона — комиссионе́р — берёт на себя обязательство перед другой стороной — комите́нтом — за вознаграждение заключить с третьим лицом или лицами одну или несколько сделок в интересах комитента, но от своего, комиссионера, имени.</p>
      </div>
      <div class="abstract2">
        <div class="abstract2__col1">
          <div class="abstract2__col1__image">
            <img src="{{ asset('assets/images/imgAbstract3.jpg') }}" alt="Red Jacket">
          </div>
          <div class="abstract2__col1__container">
            <div class="abstract2__col1__container__text">
              <p>Условия приема товаров на реализацию</p>
            </div>
            <img class="abstract2__col1__container__image" src="{{ asset('assets/images/arrow-red.svg') }}" alt="Red Arrow Down">
          </div>
        </div>
        <div class="abstract2__col2">
          <div class="abstract2__col2__redcube"></div>
          <div class="abstract2__col2__blackcube"></div>
        </div>
      </div>
    </article>
    <article class="conditions" id="conditions">
      <div class="conditions__intro">
        <p>Комитент сдает на реализацию комиcсионеру предметы личного гардероба на следующих условиях*:
        </p>
      </div>
      <div class="conditions__intro__line"></div>
      <div class="conditions__headtext">общие положения</div>
      <div class="conditions__maintext">
        <p>Срок реализации принятого товара – 60 дней с момента составления договора и фактического приема товара.</p>
        <div class="smallredcube"></div>
        <p>Комитент и Комиссионер договариваются о максимально релевантной стоимости каждой позиции товара. Список товаров, их состояние на момент приемки и стоимость отображаются в Акте приема товаров.</p>
        <div class="smallredcube"></div>
        <p>Уценка товара. Магазин ежемесячно проводит распродажу, с целью стимуляции продаж. Комитент обязан принять участие в распродаже, в случае, если принятый товар находится на реализации более 30 календарных дней. Сумма скидки составит -30% от изначальной стоимости товара. Участие в распродаже остается на усмотрении комитента, если срок реализации товара менее 30 календарных дней. Все прочие форматы уценки товара оговариваются отдельно и требуют согласования с Комитентом.</p>
        <div class="smallredcube"></div>
        <p>В случае, если Комитент захочет забрать принятые на реализацию вещи раньше оговоренного срока (60 календарных дней) он выплачивает Комиссионеру минимальный тариф за хранение, который составит 10% от согласованной стоимости товара.</p>
        <div class="conditions__maintext__line"></div>
        <p class="conditions__maintext__addition">*Согласно ПОСТАНОВЛЕНИЮ Nr. 1010 от 31.10.1997 об утверждении Правил комиссионной торговли.
        </p>
      </div>
    </article>
    <article class="numbers">
      <div class="numbers__container">
        <div class="numbers__percent">25<span>%</span></div>
        <p>от финальной стоимости товара составят комиссионные магазина</p>
        <div class="numbers__percent">10<span>%</span></div>
        <p>составлит подоходный налог*, который будет удержан из остатка суммы после вычета комиссионных</p>
        <div class="numbers__line"></div>
        <p>*Подоходный налог. Статья 90-1. Окончательное удержание налога из некоторых видов доходов. Каждый комиссионер удерживает налог в размере 10 процентов из выплат, осуществленных в пользу физического лица.</p>
      </div>
    </article>
    <article class="formula">
      <div class="formula__headtext">
        <h3 class="formula__headtext__small"><span>ФОРМУЛА</span></h3>
        <h1 class="formula__headtext__big">
          расчета
        </h1>
      </div>
      <div class="formula__scheme">
        <div class="formula__scheme__item">
          <div class="formula__scheme__item__container">
            <div class="formula__scheme__item__container__brace">
              <div class="formula__scheme__item__container__brace__big">{</div>
              <div class="formula__scheme__item__container__brace__small">продажная цена товара</div>
              <div class="formula__scheme__item__container__brace__big">}</div>
            </div>
            <p>Например 1000 леев</p>
          </div>
        </div>
        <div class="formula__scheme__item">
          <div class="formula__scheme__item__sign">-</div>
          <div class="formula__scheme__item__container">
            <div class="formula__scheme__item__container__brace">
              <div class="formula__scheme__item__container__brace__big">{</div>
              <div class="formula__scheme__item__container__brace__small">25% комиссиона HolyChiс</div>
              <div class="formula__scheme__item__container__brace__big">}</div>
            </div>
            <p>1000 - 25% = 750</p>
          </div>
        </div>
        <div class="formula__scheme__item">
          <div class="formula__scheme__item__sign">-</div>
          <div class="formula__scheme__item__container">
            <div class="formula__scheme__item__container__brace">
              <div class="formula__scheme__item__container__brace__big">{</div>
              <div class="formula__scheme__item__container__brace__small">10% подоходного налога</div>
              <div class="formula__scheme__item__container__brace__big">}</div>
            </div>
            <p>750 - 10% = 675</p>
          </div>
        </div>
        <div class="formula__scheme__item">
          <div class="formula__scheme__item__sign">=</div>
          <div class="formula__scheme__item__container">
            <div class="formula__scheme__item__container__brace">
              <div class="formula__scheme__item__container__brace__big">{</div>
              <div class="formula__scheme__item__container__brace__small">конечная выплата комитенту</div>
              <div class="formula__scheme__item__container__brace__big">}</div>
            </div>
            <p>675 леев</p>
          </div>
        </div>
      </div>
      <div class="formula__line"></div>
      <div class="formula__text">
        <p>Получить расчет за проданные позиции комитент может в любой день в рабочие часы магазина, после уведомления о продаже. Расчет производится наличными или на банковскую карту.</p>
      </div>
    </article>
    <article class="committent">
      <div class="committent__abstract">
        <div class="committent__abstract__col1">
          <div class="committent__abstract__col1__container">
            <div class="committent__abstract__col1__container__text">
              Права и обязанности комитента
            </div>
            <div>
              <img class="committent__abstract__col1__container__arrow" src="{{ asset('assets/images/arrow-red.svg') }}" alt="arrow red down">
            </div>
          </div>
          <div class="committent__abstract__col1__image">
            <img src="{{ asset('assets/images/imgAbstract4.jpg') }}" alt="blouse">
          </div>
        </div>
        <div class="committent__abstract__col2">
          <div class="committent__abstract__col2__image">
            <img src="{{ asset('assets/images/imgAbstract5.jpg') }}" alt="boot">
          </div>
          <div class="committent__abstract__col2__blackbox"></div>
        </div>
      </div>
      <div class="committent__headtext1">
        <h3 class="committent__headtext1__small"><span>КОМИТЕНТ</span></h3>
        <h1 class="committent__headtext1__big">
          имеет право
        </h1>
      </div>
      <div class="committent__maintext">
        <p>Принести на реализацию предметы личного гардероба (только женские), которые были заранее согласованы по фото с Комиссионером любым удобным способом (чаще личные сообщения Instagram / Facebook)</p>
        <div class="smallredcube"></div>
        <p>Предложить свою цену на каждую из позиций на обсуждение с Комиссионером. Получать информацию о статусе  сданных на реализацию позиций.</p>
        <div class="smallredcube"></div>
        <p>Получить расчет за проданные позиции комитент можно в рабочие часы студии, после уведомления о продаже. Расчет производится наличными либо зачисляется на банковскую карту Комитента (карта должна принадлежть молдавскому банку).
          Уведомление о продаже Комитент может получить 3-мя способами: mail, sms, телефонный звонок.</p>
      </div>
      <div class="committent__headtext2">
        <h3 class="committent__headtext2__small"><span>КОМИТЕНТ</span></h3>
        <h1 class="committent__headtext2__big">
          обязан
        </h1>
      </div>
      <div class="committent__maintext">
        <p>Сдать все вещи перед тем, как принести их на реализацию, в химчистку.</p>
        <div class="smallredcube"></div>
        <p>Убедится в том, что вещи не имеют дефектов, видимых следов носки и других недостатков.</p>
        <div class="smallredcube"></div>
        <p>Комитент обязан забрать вещи, которые не удалось реализовать не позднее чем через 3 рабочих дня после уведомления о возврате. Уведомление о возврате отправляется на электронный адрес Комитента, а также дублируется по SMS. В случае, если Комитент не приходит в течении 3-х дней за возвратом Комиссионер имеет право взымать таксу за хранение не реализованного товара в размере 50 леев за каждый день хранения. Если Комитент не забирает возврат в течении 2-х недель Комиссионер имеет право передать возврат на благотворительные нужды (а именно в благотворительный магазин MESTO).</p>
      </div>
    </article>
    <article class="commissioner">
      <div class="commissioner__abstract">
        <div class="commissioner__abstract__col1">
          <div class="commissioner__abstract__col1__redcube"></div>
          <div class="commissioner__abstract__col1__image">
            <img src="{{ asset('assets/images/imgAbstract6.jpg') }}" alt="woman with bag">
          </div>
        </div>
        <div class="commissioner__abstract__col2">
          <div class="commissioner__abstract__col2__blackcube"></div>
        </div>
        <div class="commissioner__abstract__col3">
          <div class="commissioner__abstract__col3__image">
            <img src="{{ asset('assets/images/imgAbstract7.jpg') }}" alt="shoe">
          </div>
          <div class="commissioner__abstract__col3__text">Права и обязанности комиссионера</div>
        </div>
      </div>
      <div class="commissioner__headtext1">
        <h3 class="commissioner__headtext1__small"><span>КОМИССИОНЕР</span></h3>
        <h1 class="commissioner__headtext1__big">
          имеет право
        </h1>
      </div>
      <div class="commissioner__maintext">
        <p>Выборочно принимать согласованный по фото товар.</p>
        <div class="smallredcube"></div>
        <p>Отказать в приемке товара в случае обнаружения дефектов, видимых следов носки и т.д.</p>
        <div class="smallredcube"></div>
        <p>Рекомендовать Комитенту релевантную стоимость, по которой товар предположительно удастся максимально быстро продать.</p>
        <div class="smallredcube"></div>
        <p>Комиссионер оставляет на свое усмотрение проведение фотосессии товара и публикацию на странице в социальных сетях.</p>
      </div>

      <div class="commissioner__headtext2">
        <h3 class="commissioner__headtext2__small"><span>КОМИССИОНЕР</span></h3>
        <h1 class="commissioner__headtext2__big">
          обязан
        </h1>
      </div>
      <div class="commissioner__maintext">
        <p>Следить за сохранностью принятых на реализацию позиций.</p>
        <div class="smallredcube"></div>
        <p>Выставить принятые позиции в физическом магазине по адресу г. Кишинев, ул. М.Г. Бэнулеску Бодони, 27 (вход с ул. Букурешть)</p>
        <div class="smallredcube"></div>
        <p>Обеспечить возможность покупки принятых на реализацию позиций в рабочие часы магазина, а именно по будням с 11:00 до 19:00, в выходные дни с 12:00 до 18:00.</p>
        <div class="smallredcube"></div>
        <p>В течении 5 рабочих дней сообщить Комитенту о том, что одна или несколько взятых на реализацию позиций были проданы.</p>
      </div>
    </article>
    <article class="addictions">
      <div class="addictions__brands">
        <div class="addictions__brands__container">
          <div class="addictions__brands__headtext">brand list</div>
          <p>В Holy Chic  вы можете продавать одежду и аксессуары, которые вы почти не носили, или новые вещи известных мировых брендов из категории <b>Middle-Premium, Premium, Lux.</b></p>
        </div>
      </div>
      <div class="addictions__blacklist">
        <div class="addictions__blacklist__container">
          <div class="addictions__blacklist__headtext">black list</div>
          <p class="addictions__blacklist__mediumtext">Мы не работаем с вещами из категории масс-маркет, а также
            hand-made и неизвестных марок. Нам не стоит предлагать такие бренды как:
            </p>
          <p class="addictions__blacklist__smalltext">ZARA,MASSIMO DUTTI, MANGO, Karen Millen, H&M, TOPSHOP, STRADIVARIUS, MONSOON, ABERCROMBIE & FITCH, OASIS, TATUUM, ASOS, TOMMY HILFIGER, CARLO PAZOLINI, CORSO COMO, ALDO, ADAMI, CENTRO, ЭКОНИКА, PROMOD, PULL&BEAR, MOHITO, TERRANOVA, MARKS & SPENCER, BENETTON, BERSHKA, GUESS, PEPE, JLO, BEBE, базовые линии: CONVERSE, ADIDAS, NIKE, REEBOK и пр.</p>
        </div>
      </div>
      <div class="addictions__replica">
        <div class="addictions__replica__headtext">В HolyChic запрещена продажа любого рода копий и реплик
        </div>
        <div class="addictions__replica__image">
          <img src="{{ asset('assets/images/bags.jpg') }}" alt="bags control">
        </div>
        <div class="addictions__replica__maintext">
          <p>Все товары, принятые на комиссию проходят проверку на подлинность и соответсвие заявленым брендам. В случае с люксовыми сумками, мы проводим обязательную аутентификацию посредством гаджета Entrupy.</p>
          <p>Если вы принесли нам подделку и мы это выявили – вы оплачивате стоимость аутентика (30 евро), мы возвращаем вам изделие и вносим в список потенциально неблагонадежных поставщиков.</p>
          <p>В случае, если сумка является оригиналом и Entrupy это подтведжает сертификатом подлинности, аутентик яляется бесплатным.</p>
        </div>
      </div>
      <div class="addictions__contract">
        <div class="addictions__contract__container">
        <p>Подписав договор комиссии, вы подтверждаете,что ознакомились и согласились с условиями приема товаров на реализацию.</p>
        </div>
      </div>
    </article>
@endsection