<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Page;

class PageController extends Controller
{
    public function index()
    {
        //$textPage1 = Page::find(1)->content;
        //$textPage2 = Page::find(2)->content;
        //return view('index', compact('textPage1', 'textPage2'));
        return view('index');
    }
}
